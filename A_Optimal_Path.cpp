#include<bits/stdc++.h>
#define pb push_back
#define all(x) (x).begin(), (x).end()
#define ll long long int
#define ld long double
#define fast_cin() ios_base::sync_with_stdio(false); cin.tie(NULL)
#define inarr(arr,n); for(ll i=0;i<n;i++) cin >> arr[i];
#define outarr(arr,n); for(ll i=0;i<n;i++) cout<<arr[i]<<" ";
#define PI 3.141592653589793238462643383279502884197169399375105820974944592307816406286 
const ll MOD = 1e9+7; // 998244353
const ll INF = 1000000009;
const ll MAXN = 300050;
using namespace std;
typedef vector<ll> vll;
using namespace std;


void solve()
{
    ll n,m;
    cin>>n>>m;
    // ll a[n][m];
    // for(ll i=0;i<n;i++)
    // {
    //     for(ll j=0;j<m;j++)
    //     {
    //         a[i][j] = (i-1+1)*m+(j+1);
    //     }
    // }
    // // ll dp[n][m];
    // vector<vll> dpmax(n,vll(m,-1e16)), dpmin(n,vll(m,INT_MAX));
    // dpmax[0][0] = dpmin[0][0] = a[0][0];
    // for(ll i=1;i<n;i++)
    // {
    //     // dpmax[i][0] = a[i][0] + dpmax[i-1][0];
    //     dpmin[i][0] = a[i][0] + dpmin[i-1][0];
    // }

    // for(ll i=1;i<m;i++)
    // {
    //     // dpmax[0][i] = a[0][i] + dpmax[0][i-1];
    //     dpmin[0][i] = a[0][i] + dpmin[0][i-1];
    // }

    // for(ll i=1;i<n;i++)
    // {
    //     for(ll j=1;j<m;j++)
    //     {
    //         dpmin[i][j] = min(dpmin[i-1][j]+a[i][j], dpmin[i][j-1]+a[i][j]);
    //     }
    // }
    // cout<<dpmin[n-1][m-1]<<endl;
    ll ans=0;
    for(ll i=1;i<=m;i++)
    {
        ans+=i;
    }
    for(ll i=2;i<=n;i++)
    {
        ans+=  i * m; 
    }
    cout<<ans<<endl;

}


int main()
{
    fast_cin();
    ll t=1;
    cin>>t;

    while(t--)
    {
        solve();
    }  
}
    
    