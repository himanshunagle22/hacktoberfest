#include<bits/stdc++.h>
#define pb push_back
#define all(x) (x).begin(), (x).end()
#define ll long long int
#define ld long double
#define fast_cin() ios_base::sync_with_stdio(false); cin.tie(NULL)
#define inarr(arr,n); for(ll i=0;i<n;i++) cin >> arr[i];
#define outarr(arr,n); for(ll i=0;i<n;i++) cout<<arr[i]<<" ";
#define PI 3.141592653589793238462643383279502884197169399375105820974944592307816406286 
const ll MOD = 1e9+7; // 998244353
const ll INF = 1000000009;
const ll MAXN = 300050;
using namespace std;
typedef vector<ll> vll;
using namespace std;

ll flag = 0;

void dfs(vll &a, vll &b, vll &vis, map<ll,ll> &posa, map<ll,ll> &posb,ll idx, ll turn,set<ll> &s)
{   
    
    if(s.find(idx)!=s.end())
    {
        flag=1;
    }
    if(vis[idx]==1)
    {
        // cout<<endl<<"Return : "<<idx<<endl;
        return;
    }
    // cout<<idx<<" ";
    vis[idx] = 1;
    idx = posb[a[idx]];
    // cout<<"->"<<idx<<endl;
    dfs(a,b,vis,posa,posb,idx,turn,s);
}

void solve()
{
    ll a,b,c,d;
    cin>>a>>b>>c>>d;
    ll ans=0;
    if(b>a)
        ans++;
    if(c>a) ans++;
    if(d>a) ans++;
    cout<<ans<<endl;
}


int main()
{
    fast_cin();
    ll t=1;
    cin>>t;

    while(t--)
    {
        solve();
    }  
}
    
    